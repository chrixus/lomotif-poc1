//
//  VideoPreviewViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import AVKit
import AssetsLibrary
import Photos
import AVFoundation

class VideoPreviewViewController: UIViewController {

    @IBOutlet weak var view_videoplayer: VideoView!
    @IBOutlet weak var start_btn: UIButton!
    @IBOutlet weak var videoDurationLbl: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var videoVolumeSlider: UISlider!
    @IBOutlet weak var musicVolumeSlider: UISlider!
    @IBOutlet weak var videoVolumeLabel: UILabel!
    @IBOutlet weak var musicVolumeLabel: UILabel!
    @IBOutlet weak var videoVolumeValue: UILabel!
    @IBOutlet weak var musicVolumeValue: UILabel!
    @IBOutlet weak var export_btn: UIButton!
    
    let videoUrl = Bundle.main.url(forResource: "Movie3", withExtension: "mp4")!
    let audioUrl = URL(string: "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview122/v4/8a/dd/1f/8add1f4d-142c-1317-250d-ff6370962fb8/mzaf_7601694821840779604.plus.aac.p.m4a")!
    
    private var videoVolume: Float = 0.5
    private var musicVolume: Float = 0.5

    private var mergedAudioVideoURL: URL?
    
    private var audioMix: AVAudioMix?
    private var composition: AVComposition?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_videoplayer.player = AVPlayer(playerItem: nil)
        
        start_btn.layer.cornerRadius = 8
        start_btn.clipsToBounds = true
        videoVolumeLabel.isHidden = true
        videoVolumeValue.isHidden = true
        videoVolumeSlider.isHidden = true
        musicVolumeLabel.isHidden = true
        musicVolumeSlider.isHidden = true
        musicVolumeValue.isHidden = true
        
        export_btn.isHidden = true
        
        videoVolumeSlider.value = videoVolume
        musicVolumeSlider.value = musicVolume
        
        videoVolumeValue.text = String(format: "%.2f", videoVolume)
        musicVolumeValue.text = String(format: "%.2f", musicVolume)
        
        videoVolumeSlider.addTarget(self, action: #selector(videoVolSliderDidChange(_:)), for: .valueChanged)
        musicVolumeSlider.addTarget(self, action: #selector(musicVolSliderDidChange(_:)), for: .valueChanged)
        videoVolumeSlider.addTarget(self, action: #selector(videoVolSliderDidEndSliding(_:)), for: .touchUpInside)
        musicVolumeSlider.addTarget(self, action: #selector(musicVolSliderDidEndSliding(_:)), for: .touchUpInside)
        
    }
    
    @IBAction func startBtnPressed(_ sender: Any) {
        mergeFilesWithUrl()
    }
    
    @IBAction func exportBtnPressed(_ sender: Any) {
        exportMergedVideosWithURL()
    }
    
    @objc func videoVolSliderDidChange(_ sender: Any) {
        videoVolume = videoVolumeSlider.value
        videoVolumeValue.text = String(format: "%.2f", videoVolume)
    }
    
    @objc func musicVolSliderDidChange(_ sender: Any) {
        musicVolume = musicVolumeSlider.value
        musicVolumeValue.text = String(format: "%.2f", musicVolume)
    }
    
    @objc func videoVolSliderDidEndSliding(_ sender: Any) {
        videoVolume = videoVolumeSlider.value
        mergeFilesWithUrl()
    }
    
    @objc func musicVolSliderDidEndSliding(_ sender: Any) {
        musicVolume = musicVolumeSlider.value
        mergeFilesWithUrl()
    }
    
    func mergeFilesWithUrl() {
        let audioMix: AVMutableAudioMix = AVMutableAudioMix()
        var audioMixParam: [AVMutableAudioMixInputParameters] = []
        
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionBackTrack: [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        export_btn.isHidden = false
        start_btn.isHidden = true
        
        let aVideoAsset: AVAsset = AVAsset(url: videoUrl)
        let aAudioAsset: AVAsset = AVAsset(url: audioUrl)
        
        let aVideoAudioAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.audio)[0]
        let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        let videoParam: AVMutableAudioMixInputParameters = AVMutableAudioMixInputParameters(track: aVideoAudioAssetTrack)
        let musicParam: AVMutableAudioMixInputParameters = AVMutableAudioMixInputParameters(track: aAudioAssetTrack)
        
        
        if let track = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: CMPersistentTrackID()) {
            mutableCompositionVideoTrack.append(track)
        }
        
        if let track = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: CMPersistentTrackID()) {
            mutableCompositionAudioTrack.append(track)
        }
        
        if let track = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: CMPersistentTrackID()) {
            mutableCompositionBackTrack.append(track)
        }
        
        videoParam.trackID = mutableCompositionBackTrack[0].trackID
        musicParam.trackID = mutableCompositionAudioTrack[0].trackID
        
        //Set final volume of the audio record and the music
        videoParam.setVolume(videoVolume, at: .zero)
        musicParam.setVolume(musicVolume, at: .zero)
        
        //Add setting
        audioMixParam.append(musicParam)
        audioMixParam.append(videoParam)
        
        do {
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: .zero, duration: aVideoAsset.duration), of: aVideoAssetTrack, at: .zero)
            
            try mutableCompositionBackTrack[0].insertTimeRange(CMTimeRangeMake(start: .zero, duration: aVideoAsset.duration), of: aVideoAudioAssetTrack, at: .zero)
            
            /* The audio file has a longer duration than the video file, so the duration is taken from videoAsset duration instead of audioAsset duration*/
            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: .zero, duration: aVideoAsset.duration), of: aAudioAssetTrack, at: .zero)
        } catch {
            
        }
        
        //Add parameter
        audioMix.inputParameters = audioMixParam
        
        totalVideoCompositionInstruction.timeRange = CMTimeRange(start: .zero, duration: aVideoAssetTrack.timeRange.duration)
        let mutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTime(value: 1, timescale: 30)
        
        let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: aVideoAudioAssetTrack)
        layerInstruction.setTransform(aVideoAudioAssetTrack.preferredTransform, at: .zero)
        
        let instructions = AVMutableVideoCompositionInstruction()
        instructions.layerInstructions = [layerInstruction]
        
        mutableVideoComposition.instructions = [instructions]
        mutableVideoComposition.renderSize = CGSize(width: 1280, height: 720)
        
        self.composition = mixComposition
        self.audioMix = audioMix
        
        let playerItem = AVPlayerItem(asset: mixComposition)
        playerItem.audioMix = audioMix
//        playerItem.videoComposition = mutableVideoComposition
        view_videoplayer.player?.replaceCurrentItem(with: playerItem)
        view_videoplayer.setVideoFillMode(.aspectFill)
        
        view_videoplayer.player?.play()
        
        videoVolumeLabel.isHidden = false
        videoVolumeValue.isHidden = false
        videoVolumeSlider.isHidden = false
        musicVolumeLabel.isHidden = false
        musicVolumeSlider.isHidden = false
        musicVolumeValue.isHidden = false
        
        view_videoplayer.bringSubviewToFront(videoDurationLbl)
        let videoDuration = mixComposition.duration.seconds
        
        self.videoDurationLbl.text = String(format: "%.2fs", videoDuration)
    }
    
    func exportMergedVideosWithURL() {
        guard let composition = composition,
            let audioMix = audioMix else {
                print("error")
                return
        }
        
        let savePathUrl = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("movie.mov")
        
        do {
            try FileManager.default.removeItem(at: savePathUrl)
        } catch { print(error.localizedDescription) }
        
        guard let assetExport = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetMediumQuality) else {
            print("error: asset export is null")
            return
        }
        
        assetExport.outputFileType = .mov
        assetExport.audioMix = audioMix
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        let exportProgressBarTimer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) {timer in
            self.progressBar.progress = assetExport.progress
        }
        
        let alert = UIAlertController(title: nil, message: "Exporting...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = .gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        dismiss(animated: false, completion: nil)
        
        assetExport.exportAsynchronously { () -> Void in
            exportProgressBarTimer.invalidate()
            switch assetExport.status {
            case .completed:
                self.view_videoplayer.player?.play()
                
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: savePathUrl)
                }, completionHandler: { (saved, error) in
                    if saved {
                        let alertController = UIAlertController(title: "Your video was successfully saved", message: nil, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                })
                
                print("success")
            case  .failed:
                print("failed \(assetExport.error)")
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error)")
            default:
                print("complete")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
