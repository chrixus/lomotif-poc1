//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"
fileprivate let itemsPerRow: CGFloat = 3
fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

class PictureFeedCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var imageCollectionView: UICollectionView!
    
    private var hits = [Image]()
    var pageNumber = Int()
    var isPageRefreshing: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageNumber = 1
        downloadJson()
        
        
        // Uncomment the following line to preserve selection between presentations
         self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        imageCollectionView.register(UINib(nibName: "ViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hits.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ViewCollectionViewCell else {return UICollectionViewCell()}
        
        let imageView = cell.cellImageView
        
        imageView?.sd_setShowActivityIndicatorView(true)
        imageView?.sd_setIndicatorStyle(.gray)
        imageView?.sd_setImage(with: URL(string: hits[indexPath.row].previewURL), placeholderImage: nil)
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */
    
    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */
    
    
    func downloadJson(){
        
        let strPageNumber: String = String(describing: pageNumber )
        
        print(strPageNumber)
        
        let url = URL(string: "https://pixabay.com/api/?key="+ApiDetails.apiKey+"&q=flower&page="+strPageNumber)
        print(url!)
        
        guard let downloadURL = url else { return }
        URLSession.shared.dataTask(with: downloadURL) {data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("Something is wrong")
                return
            }
            print("Downloaded")
            do
            {
                let decoder = JSONDecoder()
                let downloadedImages = try decoder.decode(Hits.self, from: data)
                self.hits.append(contentsOf: downloadedImages.hits)
                DispatchQueue.main.async {
                    self.imageCollectionView.reloadData()
                }
            } catch {
                print("Something wrong after downloaded")
                print("\(error)")
            }
            }.resume()
    }
    
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            // To get equal width for each item of the collection view with a padding space.
            //paddingSpace = 'horizontal space for each
            
            let paddingSpace = 4 * (itemsPerRow - 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            
            return CGSize(width: widthPerItem, height: widthPerItem)
        }
        
        //3
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            insetForSectionAt section: Int) -> UIEdgeInsets {
            return sectionInsets
        }
        
        // 4
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 4
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(imageCollectionView.contentOffset.y >= (imageCollectionView.contentSize.height - imageCollectionView.bounds.size.height)) {
            if(isPageRefreshing == false && pageNumber < 3 && hits.count == 20*pageNumber) {
                isPageRefreshing = true
            
                pageNumber += 1
                downloadJson()
                isPageRefreshing = false
            }
        }
    }
    
}


