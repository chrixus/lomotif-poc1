//
//  Image.swift
//  iOSTechnicalTest
//
//  Created by Chrissandro on 18/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import Foundation

class Hits: Codable {
    let hits: [Image]
    
    init(hits: [Image]){
        self.hits = hits
    }
}

class Image: Codable {
    let previewURL: String
    let previewHeight: Int
    let previewWidth: Int
    
    init(previewURL: String, previewHeight: Int, previewWidth: Int){
        self.previewURL = previewURL
        self.previewWidth = previewWidth
        self.previewHeight = previewHeight
    }
}
